package com.flipkart.app.ecom.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "FLIPKART_INFO")

public class Flipkart implements Serializable {
@Id						//making it as primary column always unique
@Column(name = "id")
private long id;

@Column(name = "name")
private String name;

@Column(name = "establishedyear")
private long establishedyear ;

@Column(name = "brandvalue")
private String brandvalue;

@Column(name = "origincountry")
private String  origincountry;

public long getId() {
	return id;
}

public void setId(long id) {
	this.id = id;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public long getEstablishedyear() {
	return establishedyear;
}

public void setEstablishedyear(long establishedyear) {
	this.establishedyear = establishedyear;
}

public String getBrandvalue() {
	return brandvalue;
}

public void setBrandvalue(String brandvalue) {
	this.brandvalue = brandvalue;
}

public String getOrigincountry() {
	return origincountry;
}

public void setOrigincountry(String origincountry) {
	this.origincountry = origincountry;
}
	
	
}
