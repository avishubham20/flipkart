 package com.flipkart.app.ecom.Repository;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.flipkart.app.ecom.entity.Flipkart;

public class EcomRepository {

	public void saveFlipkartDetails(Flipkart ecom) {
		try {
		Configuration cfg = new Configuration();
		cfg.configure();
		//cfg.addAnnotatedClass(Movie.class);
		SessionFactory sessionFactory = cfg.buildSessionFactory();   
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		session.save(ecom);
		transaction.commit();
	}catch(HibernateException e) {
		
}
	}
}
